let buttons = document.querySelectorAll('button');
let options = ['papel', 'tesoura', 'pedra'];
const target = document.getElementById('result');

function computerWins(choice, computerChoice) {
   target.innerHTML = '';
   let text = choice + ' contra ' + computerChoice + ': Vitória do Computador!!';
   let content = document.createTextNode(text);
   let h1 = document.createElement('h1');
   h1.appendChild(content);
   target.appendChild(h1);

   console.log('Computer Wins!!');
}

function playerWins(choice, computerChoice) {
   target.innerHTML = '';
   let text = choice + ' contra ' + computerChoice + ': Parabéns, você venceu!!';
   let content = document.createTextNode(text);
   let h1 = document.createElement('h1');
   h1.appendChild(content);
   target.appendChild(h1);

   console.log('Player Wins!!');
}

function draw(choice, computerChoice) {
   target.innerHTML = '';
   let text = choice + ' contra ' + computerChoice + ': Empate!!';
   let content = document.createTextNode(text);
   let h1 = document.createElement('h1');
   h1.appendChild(content);
   target.appendChild(h1);

   console.log('It is a Draw!!');
}

function runGame(ev) {

   const choice = ev.target.textContent;
   const idx = Math.floor(Math.random() * 3);
   const computerChoice = options[idx];

   let compResult = document.getElementsByClassName('comp');
   for (const div of compResult) {
      if (div.classList[1] === computerChoice) {
         div.style.display = 'block';
      } else {
         div.style.display = 'none';
      }
   }

   switch (choice) {
      case 'Pedra':
         switch (computerChoice) {
            case 'pedra':
               draw(choice, computerChoice);
               break;
            case 'papel':
               computerWins(choice, computerChoice);
               break;
            case 'tesoura':
               playerWins(choice, computerChoice);
               break;
         }
         break;

      case 'Papel':
         switch (computerChoice) {
            case 'pedra':
               playerWins(choice, computerChoice);
               break;
            case 'papel':
               draw(choice, computerChoice);
               break;
            case 'tesoura':
               computerWins(choice, computerChoice);
               break;
         }
         break;

      case 'Tesoura':
         switch (computerChoice) {
            case 'pedra':
               computerWins(choice, computerChoice);
               break;
            case 'papel':
               playerWins(choice, computerChoice);
               break;
            case 'tesoura':
               draw(choice, computerChoice);
               break;
         }
         break;
   }
}

buttons.forEach((button) => {
   button.addEventListener('click', runGame);
});